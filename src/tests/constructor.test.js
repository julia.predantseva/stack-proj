import Stack from '../index';


describe('constructor Stack test', () => {
  const stack = new Stack();
  const stack2 = new Stack(34);

  test('should be a constructor', () => {
    expect(Stack).toThrow();
  });

  test('is a function', () => {
    expect(Object.getPrototypeOf(stack) === Stack.prototype).toBeTruthy();
    expect(Object.getPrototypeOf(stack2) === Stack.prototype).toBeTruthy();
  });

  test('every instance should be a different', () => {
    const stack = new Stack();
    const stack2 = new Stack();

    expect(stack).not.toBe(stack2);
  });

  test('shouldnot have leng if Stack created with arguments', () => {
    const stack = new Stack([1, 5, 6, 'ds']);
    const stack2 = new Stack({ name: 'hulio' });
    const stackSize = stack.size();
    const stack2Size = stack2.size();

    expect(stackSize).toEqual(0);
    expect(stack2Size).toEqual(0);
  });
});
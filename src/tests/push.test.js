import Stack from '../index';


describe('push method tests', () => {
  test('instance should have method push', () => {
    const stack = new Stack();
    expect(stack.push).toBeInstanceOf(Function);
  });

  test('should change length of the stack after push method', () => {
    const stack = new Stack();
    stack.push('fdfd');
    stack.push(223);
    expect(stack.size()).toEqual(2);
  });

  test('should throw an error if push method not includes argumenst', () => {
    const stack = new Stack();
    expect(stack.push).toThrow();
  });

  test('should return an argument', () => {
    const stack = new Stack();
    expect(stack.push('qwa')).toEqual('qwa');
    expect(stack.push(123)).toEqual(123);
  });
});
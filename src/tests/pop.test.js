import Stack from '../index';


describe('pop method tests', () => {
  test('Stack instance has method pop', () => {
    const stack = new Stack();
    expect(stack.pop).toBeInstanceOf(Function);
  });

  test('should change length of the stack after pop method', () => {
    const stack = new Stack();
    stack.push('fdfd');
    stack.push(223);
    stack.pop();
    expect(stack.size()).toEqual(1);
  });

  test('should delete last stack item', () => {
    const stack = new Stack();
    stack.push('a');
    stack.push('b');
    const startedLastItem = stack.peek();
    stack.pop();
    const newLastItem = stack.peek();
    expect(newLastItem).not.toBe(startedLastItem);
  });

  test('should return deleted item', () => {
    const stack = new Stack();
    stack.push(123);
    stack.push(456);
    stack.push([]);
    expect(stack.pop()).toEqual([]);
    expect(stack.pop()).toEqual(456);
    expect(stack.pop()).toEqual(123);
  });

  test('should work correcty if pop method have an arguiments', () => {
    const stack = new Stack();
    stack.push(123);
    stack.push(456);
    expect(stack.pop(123)).toEqual(456);
    expect(stack.pop('32423', [], {})).toEqual(123);
  });


  test('should throw error if steck is empty', () => {
    const stack = new Stack();

    expect(stack.pop).toThrow();
    expect(() => {
      stack.pop('something');
    }).toThrow();

    stack.push(123);
    stack.pop();
    expect(stack.pop).toThrow();
  });
});
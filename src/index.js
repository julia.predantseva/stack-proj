class Stack {
    #data = null;
    #length = null;
    constructor() {
      this.#length = 0;
      this.#data = {};
    }
    push = arg => {
      if (!arg) {
        throw new SyntaxError('no arguments');
      }
      this.#data[this.#length] = arg;
      this.#length += 1;
      return arg;
    }
    pop = () => {
      if (!this.#length) {
        throw new SyntaxError('stack is empty');
      }

      const deletedData = this.#data[this.#length - 1];
      delete this.#data[this.#length - 1];
      this.#length -= 1;
      return deletedData;
    }

    peek = () => {
      if (!this.#length) {
        throw new SyntaxError('stack is empty');
      }
      return this.#data[this.#length - 1];
    }
    isEmpty =() => {
      if (!this.#length) {
        return true;
      }
      return false;
    }
    search = item => {
      for (const key in this.#data) {
        if (this.#data[key] === item) {
          return this.#length - key;
        }
      }
      return -1;
    }
    size = () => this.#length;
}

// const a = new Stack();
// const b = [1, 2, 4, 5];
// a.push(3);
// a.push('fdsvf');
// a.push([]);
// a.push({});
// a.push(b);

export default Stack;